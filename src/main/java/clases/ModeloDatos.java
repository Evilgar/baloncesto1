package clases;

import interfaces.IModeloDatos;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ModeloDatos extends BaseModeloDatos implements IModeloDatos {

    private static final Logger LOGGER = Logger.getLogger(ModeloDatos.class.getName());

    @Override
    public void abrirConexion() {
        super.abrirConexion();
    }

    @Override
    public boolean existeJugador(String nombre) {
        boolean existe = false;
        String cad = "Miguel";
        try {
            set = con.createStatement();
            rs = set.executeQuery("SELECT * FROM Jugadores");
            while (rs.next()) {
                cad = rs.getString("Nombre");
                cad = cad.trim();
                if (cad.compareTo(nombre.trim()) == 0) {
                    existe = true;
                }
            }
            rs.close();
            set.close();
        } catch (SQLException sqlE) {
            LOGGER.log(Level.SEVERE, null, sqlE);
        }
        return existe;
    }

    @Override
    public void actualizarJugador(String nombre) {
        try {
            set = con.createStatement();
            set.executeUpdate("UPDATE Jugadores SET votos=votos+1 WHERE nombre " + " LIKE '%" + nombre + "%'");
            set.close();
        } catch (SQLException sqlE) {
            LOGGER.log(Level.SEVERE, null, sqlE);
        }
    }

    @Override
    public void insertarJugador(String nombre) {
        try {
            set = con.createStatement();
            set.executeUpdate("INSERT INTO Jugadores " + " (nombre,votos) VALUES ('" + nombre + "',1)");
            set.close();
        } catch (SQLException sqlE) {
            LOGGER.log(Level.SEVERE, null, sqlE);
        }
    }

    @Override
    public void cerrarConexion() {
        super.cerrarConexion();
    }
}
