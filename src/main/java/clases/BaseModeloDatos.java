package clases;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

public class BaseModeloDatos {

    protected Connection con = null;
    protected Statement set = null;
    protected ResultSet rs = null;
    protected static final Logger LOGGER = Logger.getLogger(BaseModeloDatos.class.getName());

    public void abrirConexion() {

        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            String dbHost = System.getenv().get("DATABASE_HOST");
            String dbPort = System.getenv().get("DATABASE_PORT");
            String dbName = System.getenv().get("DATABASE_NAME");
            String dbUser = System.getenv().get("DATABASE_USER");
            String dbPass = System.getenv().get("DATABASE_PASS");

            String url = dbHost + ":" + dbPort + "/" + dbName;
            con = DriverManager.getConnection(url, dbUser, dbPass);
        } catch (ClassNotFoundException | SQLException e) {
            LOGGER.log(Level.SEVERE, "error al abrir la conexión a BBDD", e);
        }
    }

    public void cerrarConexion() {
        try {
            con.close();
        } catch (SQLException sqlE) {
            LOGGER.log(Level.SEVERE, "error al cerrar la conexión a BBDD", sqlE);
        }
    }
}
