package clases;

import interfaces.IModeloDatosBorrarVotos;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;


public class ModeloDatosBorradoVotos extends BaseModeloDatos implements IModeloDatosBorrarVotos {
    private static final Logger LOGGER = Logger.getLogger(ModeloDatosBorradoVotos.class.getName());

    @Override
    public void abrirConexion() {
        super.abrirConexion();
    }

    @Override
    public void deleteVotos() {
        try {
            set = con.createStatement();
            set.executeUpdate("UPDATE Jugadores SET votos = 0");
            set.close();
        } catch (SQLException sqlE) {
            LOGGER.log(Level.SEVERE, null, sqlE);
        }
    }

    @Override
    public void cerrarConexion() {
        super.cerrarConexion();
    }
}