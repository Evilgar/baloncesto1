package clases;

import interfaces.IModeloDatosBorrarVotos;
import java.io.IOException;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class BorrarVotosServlet extends HttpServlet {
    
    private static final String BORRAR = "Borrar votos";
    private IModeloDatosBorrarVotos mdbv;

    @Override
    public void init(ServletConfig cfg) throws ServletException {
        
        mdbv = new ModeloDatosBorradoVotos();
        mdbv.abrirConexion();
    }

    @Override
    public void service(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        String valueBoton = req.getParameter("B3");
        if (valueBoton.equals(BORRAR)) {
            mdbv.deleteVotos();
        }
        res.sendRedirect(res.encodeRedirectURL("index.html"));
    }

    @Override
    public void destroy() {
        mdbv.cerrarConexion();
        super.destroy();
    }
}
