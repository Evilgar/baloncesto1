package clases;

import interfaces.IModeloDatosListar;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class VerJugadoresYVotosServlet extends HttpServlet {

    private IModeloDatosListar mdl;
    private Map<String, ArrayList<String>> jugadoresYVotos = new LinkedHashMap<String, ArrayList<String>>();
    private static final String VER = "Ver Votaciones";

    @Override
    public void init(ServletConfig cfg) throws ServletException {
        jugadoresYVotos = null;
        mdl = new ModeloDatosListado();
        mdl.abrirConexion();
    }

    @Override
    public void service(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        HttpSession sesion = req.getSession(true);
        jugadoresYVotos = (LinkedHashMap<String, ArrayList<String>>) mdl.listar();
        sesion.setAttribute("jugadoresYVotos", jugadoresYVotos);
        res.sendRedirect(res.encodeRedirectURL("VerVotos.jsp"));
    }

    @Override
    public void destroy() {
        mdl.cerrarConexion();       
        super.destroy();
    }
}
