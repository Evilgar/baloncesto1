package clases;

import interfaces.IModeloDatosListar;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;


public class ModeloDatosListado extends BaseModeloDatos implements IModeloDatosListar {
    
    private static final Logger LOGGER = Logger.getLogger(ModeloDatosListado.class.getName());
    Map<String, ArrayList<String>> datos = new LinkedHashMap<String, ArrayList<String>>();
 
    @Override
    public void abrirConexion() {
        super.abrirConexion();
    }

    @Override
    public Map<String, ArrayList<String>> listar() {
        try {
            set = con.createStatement();
            String query = "SELECT * FROM Jugadores ORDER BY id ASC;";
            rs = set.executeQuery(query);
            while (rs.next()) {
                String id = Integer.toString(rs.getInt("id"));
                String nombre = rs.getString("nombre");
                String votos = Integer.toString(rs.getInt("votos"));
                ArrayList<String> row = new ArrayList();
                row.add(nombre);
                row.add(votos);
                datos.put(id, row);
            }
            rs.close();
            set.close();
        } catch (SQLException sqlE) {
            LOGGER.log(Level.SEVERE, null, sqlE);
        }
        return datos;
    }

    @Override
    public void cerrarConexion() {
        super.cerrarConexion();
    }
}
