/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaces;

import java.util.ArrayList;
import java.util.Map;

/**
 *
 * @author eva
 */
public interface IModeloDatosListar {

    void abrirConexion();

    void cerrarConexion();

    Map<String, ArrayList<String>> listar();
    
}
