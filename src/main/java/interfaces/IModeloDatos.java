/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaces;

/**
 *
 * @author eva
 */
public interface IModeloDatos {

    void abrirConexion();

    void actualizarJugador(String nombre);

    void cerrarConexion();

    boolean existeJugador(String nombre);

    void insertarJugador(String nombre);
    
}
