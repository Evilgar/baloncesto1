<%@page import="java.util.LinkedHashMap"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>

<html>
    <head>
        <title>Votacion mejor jugador liga ACB</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" >
    </head>
    <body class="bg-primary">
        <header class="row mb-4">
            <div class="col">
                <nav class="navbar navbar-expand-md navbar-dark bg-dark">
                    <a class="navbar-brand" href="index.html">
                        <span class="d-none d-lg-inline"> Inicio </span>
                    </a>
                </nav>
            </div>
        </header>
        <br>
        <div class="row mb-4">
            <div class="col">
                <h3 class="text-warning text-center">Votaciones</h3>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-auto">
                <div class="table-responsive-md text-center">
                    <table  class="table table-dark table-striped table-hover table-borderless">
                        <thead class="thead-light">
                            <tr>
                                <th></th><th>Jugador</th><th>Votos</th></tr>
                        </thead>
                        <tbody>
                            <%
                                String key = null;
                                Map<String, ArrayList<String>> listado = (LinkedHashMap<String, ArrayList<String>>) session.getAttribute("jugadoresYVotos");
                                for (Map.Entry<String, ArrayList<String>> entry : listado.entrySet()) {
                                    key = entry.getKey();
                                    ArrayList<String> value = entry.getValue();
                            %>
                            <tr>
                                <td id="id"><%=key%></td> 
                                <td id="jugador"><%=value.get(0)%></td> 
                                <td id="votos"><%=value.get(1)%></td></tr>
                                <%
                                    }
                                %>
                        </tbody>
                    </table> 
                </div> 
            </div> 
        </div> 
        <br>
    </body>
</html>
