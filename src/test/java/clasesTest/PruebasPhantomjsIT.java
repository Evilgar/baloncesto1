package clasesTest;

import java.util.List;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.junit.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.openqa.selenium.phantomjs.PhantomJSDriverService;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class PruebasPhantomjsIT {

    private static WebDriver driver = null;

    @BeforeClass
    public static void setUpClass() {
        DesiredCapabilities caps = new DesiredCapabilities();
        caps.setJavascriptEnabled(true);
        caps.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY, "/usr/bin/phantomjs");
        caps.setCapability(PhantomJSDriverService.PHANTOMJS_CLI_ARGS, new String[]{"--web-security=false", "--ignore-ssl-errors=yes"});
        driver = new PhantomJSDriver(caps);
    }

    @Test
    public void tituloIndexTest() {
        driver.navigate().to("http://localhost:8080/baloncesto1/");
        assertEquals("Votacion mejor jugador liga ACB", driver.getTitle(), "El titulo no es correcto");
    }

    @Test
    public void botonesBorrarYVerTest() {
        driver.navigate().to("http://localhost:8080/baloncesto1/");
        driver.findElement(By.name("B3")).click();
        driver.findElement(By.name("B4")).click();
        List<WebElement> votos = driver.findElements(By.id("votos"));
        for (int i = 0; i < votos.size(); i++) {
            String value = (votos.get(i).getText());
            assertEquals(value, Integer.toString(0));
        }
    }

    @Test
    public void pruebainsertar() {
        driver.navigate().to("http://localhost:8080/baloncesto1/");
        driver.findElement(By.name("txtNombre")).sendKeys("Elsa Capunta");
        driver.findElement(By.name("txtMail")).sendKeys("elsacapunta@hotmail.com");
        List<WebElement> radioButton = driver.findElements(By.name("R1"));
        for (int i = 0; i < radioButton.size(); i++) {
            String valueButton = radioButton.get(i).getAttribute("value");
            if (valueButton.equals("Otros")) {
                radioButton.get(i).click();
            }
        }
        String name = "Miguel";
        boolean existe = false;
        driver.findElement(By.name("txtOtros")).sendKeys(name);
        driver.findElement(By.name("B1")).click();
        driver.findElement(By.linkText("Ir al comienzo")).click();
        driver.findElement(By.name("B4")).click();
        List<WebElement> jugadorList = driver.findElements(By.id("jugador"));
        List<WebElement> votosList = driver.findElements(By.id("votos"));
        for (WebElement element : jugadorList) {
            String nombre = element.getText();
            if (nombre.equals(name)) {
                existe = true;
            }
        }
        assertTrue(existe);
    }

    @AfterClass
    public static void teardown() {
        driver.close();
        driver.quit();
    }
}
