package clasesTest;

import clases.ModeloDatosBorradoVotos;
import java.io.FileInputStream;
import org.dbunit.IDatabaseTester;
import org.dbunit.JdbcDatabaseTester;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;



public class ModeloDatosBorradoVotosTest {

    private IDatabaseTester databaseTester;
    private IDataSet dataSet;
    private ModeloDatosBorradoVotos mdbv;

    @Before
    public void setUp() throws ClassNotFoundException, DataSetException, Exception {

        String dbHost = System.getenv().get("DATABASE_HOST");
        String dbPort = System.getenv().get("DATABASE_PORT");
        String dbName = System.getenv().get("DATABASE_NAME");
        String dbUser = System.getenv().get("DATABASE_USER");
        String dbPass = System.getenv().get("DATABASE_PASS");

        String url = dbHost + ":" + dbPort + "/" + dbName;

        databaseTester = new JdbcDatabaseTester("com.mysql.cj.jdbc.Driver",
                url, dbUser, dbPass);
        dataSet = new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/java/resources/borrarVotosEsperado.xml"));
        databaseTester.setDataSet(dataSet);
        databaseTester.onSetup();
    }

    @Test
    public void testBorrarVotosJugadores() throws Exception {
        String columna = "votos";
        mdbv = new ModeloDatosBorradoVotos();
        mdbv.abrirConexion();
        mdbv.deleteVotos();
        mdbv.cerrarConexion();
        IDatabaseConnection connection = databaseTester.getConnection();
        ITable actual = connection.createDataSet().getTable("Jugadores");
        for (int i = 0; i < actual.getRowCount(); i++) {
            assertEquals(actual.getValue(i, columna), 0);
        }
    }

    @After
      public void tearDown() throws Exception {
        databaseTester.onTearDown();
    }  
}
