package clasesTest;

import clases.ModeloDatos;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import junit.framework.TestCase;
import org.dbunit.IDatabaseTester;
import org.dbunit.JdbcDatabaseTester;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.database.QueryDataSet;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ModeloDatosTest extends TestCase {

    private IDatabaseTester databaseTester;
    private IDataSet dataSet;

    public ModeloDatosTest() {
    }

    @Before
    @Override
    public void setUp() throws DataSetException, ClassNotFoundException, FileNotFoundException, Exception {

            String dbHost = System.getenv().get("DATABASE_HOST");
            String dbPort = System.getenv().get("DATABASE_PORT");
            String dbName = System.getenv().get("DATABASE_NAME");
            String dbUser = System.getenv().get("DATABASE_USER");
            String dbPass = System.getenv().get("DATABASE_PASS");
            String url = dbHost + ":" + dbPort + "/" + dbName;

            databaseTester = new JdbcDatabaseTester("com.mysql.cj.jdbc.Driver", url, dbUser, dbPass);
            dataSet = new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/java/resources/mysqlDatoEsperados.xml"));
            databaseTester.setDataSet(dataSet);
            databaseTester.onSetup();
        }
    

        @After
        @Override
        public void tearDown() throws Exception {
            databaseTester.onTearDown();
        }

        @Test
        public void testActualizarJugador() throws Exception {
            String votosColumn = "votos";
            String nombre = "Reyes";
            IDatabaseConnection connection = databaseTester.getConnection();
            QueryDataSet partialDataSet = new QueryDataSet(connection);
            partialDataSet.addTable("Jugadores", "SELECT * FROM Jugadores");
            FlatXmlDataSet.write(partialDataSet, new FileOutputStream("src/test/java/resources/actualizarBBDD_esperados.xml"));
            IDataSet expectedDataSet = new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/java/resources/actualizarBBDD_esperados.xml"));
            ITable expectedTable = expectedDataSet.getTable("Jugadores");
            ModeloDatos md = new ModeloDatos();
            md.abrirConexion();
            md.actualizarJugador(nombre);
            md.cerrarConexion();
            ITable actual = connection.createDataSet().getTable("Jugadores");

            assertEquals((Integer.parseInt(expectedTable.getValue(2, votosColumn).toString()) + 1), ((int) actual.getValue(2, votosColumn)));
        }

        @Test
        public void testInsertarJugador() throws Exception {
            IDatabaseConnection connection = databaseTester.getConnection();
            QueryDataSet partialDataSet = new QueryDataSet(connection);
            partialDataSet.addTable("Jugadores", "SELECT * FROM Jugadores");
            FlatXmlDataSet.write(partialDataSet, new FileOutputStream("src/test/java/resources/insertarBBDD.xml"));
            IDataSet expectedDataSet = new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/java/resources/insertarBBDD.xml"));
            ITable expectedTable = expectedDataSet.getTable("Jugadores");
            String nombre = "Mariano";
            ModeloDatos md = new ModeloDatos();
            md.abrirConexion();
            md.insertarJugador(nombre);
            md.cerrarConexion();
            ITable actualTable = connection.createDataSet().getTable("Jugadores");

            assertEquals(expectedTable.getRowCount() + 1, actualTable.getRowCount());
        }
    }
